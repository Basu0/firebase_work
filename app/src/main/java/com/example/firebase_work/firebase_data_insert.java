package com.example.firebase_work;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class firebase_data_insert extends AppCompatActivity {
    EditText editText1,editText2;
    Button button;
    DatabaseReference databaseReference;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_firebase_data_insert);
        editText1=(EditText)findViewById(R.id.emailid);
        editText2=(EditText)findViewById(R.id.passwordid);
        button=(Button)findViewById(R.id.sign_in);
        databaseReference= FirebaseDatabase.getInstance().getReference("insert");
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String email= editText1.getText().toString();
                String password= editText2.getText().toString();
                String da=databaseReference.push().getKey();
                insert as=new insert(email,password);
                databaseReference.child(da).setValue(as);
                Toast.makeText(getApplication(),"Successfully Save",Toast.LENGTH_LONG).show();
            }
        });

    }


    public void show(View view) {
        Intent intent= new Intent(firebase_data_insert.this,Show_data_in_firebase.class);
        startActivity(intent);

    }
}
