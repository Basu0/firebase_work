package com.example.firebase_work;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.firebase.auth.FirebaseAuth;

public class profile_activitey extends AppCompatActivity {
   FirebaseAuth firebaseAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_activitey);
        firebaseAuth=FirebaseAuth.getInstance();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==R.id.sign_out_id){
           FirebaseAuth.getInstance().signOut();
           finish();
            Intent inr=new Intent(profile_activitey.this,firebase_1.class);
            startActivity(inr);
        }

        if (item.getItemId()==R.id.imageuload){

            Intent inr=new Intent(profile_activitey.this,image_store.class);
            startActivity(inr);
        }

        return super.onOptionsItemSelected(item);
    }

    public void adddata(View view) {

        Intent intent=new Intent(profile_activitey.this,firebase_data_insert.class);
        startActivity(intent);
    }
}
