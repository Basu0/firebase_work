package com.example.firebase_work;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class Show_data_in_firebase extends AppCompatActivity {
    ListView listView;
    DatabaseReference databaseReference;
   public List<insert>inlist;
   public custom_adptar ff;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_data_in_firebase);
        listView=(ListView)findViewById(R.id.showlist);
        databaseReference=FirebaseDatabase.getInstance().getReference("insert");
        inlist =new ArrayList<>();
        ff  =new custom_adptar(Show_data_in_firebase.this,inlist);

     listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
         @Override
         public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

             for (int i = 0; i < listView.getCount(); i++)


             if (position==i){
                 insert insdfert=inlist.get(position);
                 Toast.makeText(getApplication(),"Hello"+insdfert.getPassord(),Toast.LENGTH_LONG).show();
             }
         }
     });

    }

    @Override
    protected void onStart() {
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                inlist.clear();
                for (DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){

                    insert inserty=dataSnapshot1.getValue(insert.class);
                    inlist.add(inserty);
                }

                listView.setAdapter(ff);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        super.onStart();
    }
}
