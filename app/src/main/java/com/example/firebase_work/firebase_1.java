package com.example.firebase_work;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class firebase_1 extends AppCompatActivity {
    EditText editText1,editText2;
    Button button;
    ProgressBar progressBar;
    private CheckBox McheckRemembar;
    private SharedPreferences mPrefrs;
    private static final String PREFS_NAME="PrefsFile";

    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_firebase_1);
        this.setTitle("SIGN IN");
        firebaseAuth = FirebaseAuth.getInstance();
        editText1=(EditText)findViewById(R.id.emailid);
        editText2=(EditText)findViewById(R.id.passwordid);
        button=(Button)findViewById(R.id.sign_in);
        McheckRemembar=(CheckBox)findViewById(R.id.chack_box);


        progressBar=(ProgressBar)findViewById(R.id.progressid);



        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

             sign_in();

               }
        });

    }



    public  void sign_in(){

        String email=editText1.getText().toString().trim();
        String password=editText2.getText().toString().trim();


        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()){

            editText1.setError("Email Not Valid");
            editText1.requestFocus();
            return;
        }

        if (password.isEmpty()){

            editText2.setError("Password Null");
            editText2.requestFocus();
            return;
        }
        if (password.length()<6){
            editText2.setError("Minimum Password 6 ");
            editText2.requestFocus();
            return;

        }


        progressBar.setVisibility(View.VISIBLE);
        firebaseAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(this, new
                OnCompleteListener<AuthResult>() {


                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        progressBar.setVisibility(View.GONE);
                        if (task.isSuccessful()){

                            Intent inr=new Intent(firebase_1.this,profile_activitey.class);
                            startActivity(inr);

                        }

                        else {
                            if (task.getException() instanceof FirebaseAuthUserCollisionException){
                                Toast.makeText(getApplication(),"Already Registar",Toast.LENGTH_LONG).show();

                            }
                            else {
                                Toast.makeText(getApplication(),"Error :"+ task.getException().getMessage(),Toast.LENGTH_LONG).show();

                            }


                        }
                    }
                });


    }


    public void ffggg(View view) {
        Intent intent=new Intent(firebase_1.this,sign_up.class);
        startActivity(intent);

    }
}
