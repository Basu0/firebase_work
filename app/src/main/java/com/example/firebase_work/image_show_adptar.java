package com.example.firebase_work;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class image_show_adptar extends RecyclerView.Adapter<image_show_adptar.MyviewHolder> {
    public Context context;
    public List<saveImage> insertList;
    private AdapterView.OnItemClickListener listener;

    public image_show_adptar(Context context, List<saveImage> insertList) {
        this.context = context;
        this.insertList = insertList;
    }

    @NonNull
    @Override
    public MyviewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater=LayoutInflater.from(context);
        View view=layoutInflater.inflate(R.layout.image_layout2,viewGroup,false);
        return new MyviewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyviewHolder myviewHolder, int i) {
        saveImage saveImage=insertList.get(i);
        myviewHolder.textView.setText(saveImage.imageName);
        Picasso.with(context).load(saveImage.getImageUrl()).placeholder(R.mipmap.ic_launcher)
                .fit().centerCrop().into(myviewHolder.imageView);

    }

    @Override
    public int getItemCount() {
        return insertList.size();
    }

    public class MyviewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView textView;
        ImageView imageView;

     public MyviewHolder(@NonNull View itemView) {
         super(itemView);
         imageView= itemView.findViewById(R.id.imageidlayout);
         textView=   itemView.findViewById(R.id.textidlayout);
         itemView.setOnClickListener(this);

     }

        @Override
        public void onClick(View v) {
         if (listener!=null){
             int postion=getAdapterPosition();

             if (postion!=RecyclerView.NO_POSITION) {

             }
         }


        }
    }

    public interface onclickitamLicanar{
        void onitamclick(int positon);
 }

  public void itamselect(AdapterView.OnItemClickListener listener){

        this.listener=listener;
 }

}
