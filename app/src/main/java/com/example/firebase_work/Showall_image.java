package com.example.firebase_work;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class Showall_image extends AppCompatActivity {
    RecyclerView recyclerView;
   public image_show_adptar imageAdptar;
   public List<saveImage>saveImages;
   DatabaseReference databaseReference;
   ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_showall_image);
        recyclerView=(RecyclerView)findViewById(R.id.recycelViewid);
        progressBar =(ProgressBar)findViewById(R.id.progID);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        saveImages =new ArrayList<>();
        databaseReference= FirebaseDatabase.getInstance().getReference("Upload");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
              for (DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){

                  saveImage saveImage=dataSnapshot1.getValue(com.example.firebase_work.saveImage.class);
                  saveImages.add(saveImage);
              }
                imageAdptar=new image_show_adptar(Showall_image.this,saveImages);
              recyclerView.setAdapter(imageAdptar);

                progressBar.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(getApplication(),"Error"+databaseError.getMessage(),Toast.LENGTH_LONG).show();
                progressBar.setVisibility(View.INVISIBLE);
            }
        });
    }
}
