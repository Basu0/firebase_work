package com.example.firebase_work;

import android.app.Activity;
import android.content.Context;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class custom_adptar extends ArrayAdapter<insert> {
   public Activity context;
   public List<insert> inlist;


    public custom_adptar(Activity context,List<insert> inlist) {
        super(context, R.layout.show_simple_list, inlist);
        this.context = context;
        this.inlist = inlist;
    }


    @Override
    public View getView(int position,  View convertView,  ViewGroup parent) {

        LayoutInflater layoutInflater=context.getLayoutInflater();

        View view=layoutInflater.inflate(R.layout.show_simple_list,null,true);
        insert insdfert=inlist.get(position);

        TextView textView1=(TextView) view.findViewById(R.id.simple_email);
        TextView textView2=(TextView) view.findViewById(R.id.simple_password);

        textView1.setText("Email:"+insdfert.getEmail());
        textView2.setText("Password:"+insdfert.getPassord());

        return view;
    }
}
