package com.example.firebase_work;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthActionCodeException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.database.DatabaseReference;


public class sign_up extends AppCompatActivity {
    EditText editText1,editText2;
    Button button;
    ProgressBar progressBar;
    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        firebaseAuth = FirebaseAuth.getInstance();
        this.setTitle("SIGN UP");
        editText1=(EditText)findViewById(R.id.email_id);
        editText2=(EditText)findViewById(R.id.passwordids);
        button=(Button)findViewById(R.id.signup_ids);
        progressBar=(ProgressBar)findViewById(R.id.progressidf);


       button.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               sinf();
           }
       });


    }

    public void sinf(){

        String email=editText1.getText().toString().trim();
        String password=editText2.getText().toString().trim();


        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()){

            editText1.setError("Email Not Valid");
            editText1.requestFocus();
            return;
        }

        if (password.isEmpty()){

            editText2.setError("Password Null");
            editText2.requestFocus();
            return;
        }
        if (password.length()<6){
            editText2.setError("Minimum Password 6 ");
            editText2.requestFocus();
            return;

        }


        firebaseAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(this, new
                OnCompleteListener<AuthResult>() {

                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        progressBar.setVisibility(View.GONE);
                        if (task.isSuccessful()){

                            Toast.makeText(getApplication(),"Registear is Success",Toast.LENGTH_LONG).show();

                             }

                        else {
                             if (task.getException() instanceof FirebaseAuthUserCollisionException){
                                 Toast.makeText(getApplication(),"Already Registar",Toast.LENGTH_LONG).show();

                             }
                             else {
                                 Toast.makeText(getApplication(),"Error :"+ task.getException().getMessage(),Toast.LENGTH_LONG).show();

                             }


                        }
                    }
                });



    }


    public void sign_up(View view) {
        Intent intent=new Intent(sign_up.this,firebase_1.class);
        startActivity(intent);
    }
}
