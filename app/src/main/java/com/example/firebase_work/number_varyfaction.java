package com.example.firebase_work;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.concurrent.TimeUnit;

public class number_varyfaction extends AppCompatActivity {
    FirebaseAuth auth;
    PhoneAuthProvider.OnVerificationStateChangedCallbacks mcallback;
    EditText e1,e2;
    Button bt1,bt2;
    String verification_code;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_number_varyfaction); FirebaseAuth auth=FirebaseAuth.getInstance();
        e1=(EditText)findViewById(R.id.numbarent);
        e2=(EditText)findViewById(R.id.otpent);
        auth=FirebaseAuth.getInstance();

        mcallback=new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {

            }

            @Override
            public void onVerificationFailed(FirebaseException e) {

            }

            @Override
            public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                super.onCodeSent(s, forceResendingToken);
                verification_code=s;
                Toast.makeText(getApplication(),"Code send to the number",Toast.LENGTH_LONG).show();

            }
        };

    }
    public void send_sms(View view){


        String num=e1.getText().toString();
        PhoneAuthProvider.getInstance().verifyPhoneNumber(num,60, TimeUnit.SECONDS,this,mcallback);
        Toast.makeText(getApplication(),"User Sing In successfull",Toast.LENGTH_LONG).show();

    }
    public void send_with_phone(PhoneAuthCredential credential){

        auth.signInWithCredential(credential).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                if (task.isSuccessful()){
                    Toast.makeText(getApplication(),"User Sing In successfull",Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void veryfi(View view){
        String otp=e2.getText().toString();
        if (verification_code.equals("")){
            verifyPhonenumber(verification_code,otp);
        }
    }

    public void  verifyPhonenumber(String veryfycode,String otp){

        PhoneAuthCredential credential= PhoneAuthProvider.getCredential(veryfycode,otp);
        send_with_phone(credential);

    }


}
