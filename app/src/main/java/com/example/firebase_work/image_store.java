package com.example.firebase_work;

import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

public class image_store extends AppCompatActivity {
    Button choseImage,loadimage,showimage;
   EditText imagename;
   ImageView imageView;
   ProgressBar progressBar;
   public Uri imageuri;
   private static final int IMAGE_REQUEST=1;
   DatabaseReference databaseReference;
   StorageReference storageReference;
   StorageTask storageTask;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_store);
        choseImage=(Button)findViewById(R.id.chooseImage);
        loadimage=(Button)findViewById(R.id.loadImage);
        showimage=(Button)findViewById(R.id.Allimage);
        imageView=(ImageView)findViewById(R.id.imageViewId);
        progressBar=(ProgressBar)findViewById(R.id.progeId);
        imagename=(EditText)findViewById(R.id.imageName_enty);

        databaseReference= FirebaseDatabase.getInstance().getReference("Upload");
        storageReference= FirebaseStorage.getInstance().getReference("Upload");

       choseImage.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               opeanfilechoore();
           }
       });

       loadimage.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               if (storageTask!=null && storageTask.isInProgress()){

                   Toast.makeText(getApplication(),"Image Upload Progressing",Toast.LENGTH_LONG).show();
               }
               else{

               loadimage();

               }
           }
       });

       showimage.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               Intent nn=new Intent(image_store.this,Showall_image.class);
               startActivity(nn);
           }
       });

    }

    public String ImageExtration(Uri imageUri){
        ContentResolver contentResolver=getContentResolver();
        MimeTypeMap mimeTypeMap=MimeTypeMap.getSingleton();
        return mimeTypeMap.getExtensionFromMimeType(contentResolver.getType(imageUri));

    }

    public void loadimage() {

        final String imagN=imagename.getText().toString().trim();
        if (imagN.isEmpty()){

            imagename.setError("Image Name Null");
            imagename.requestFocus();
            return;
        }

        StorageReference rf=storageReference.child(System.currentTimeMillis()+"."+ImageExtration(imageuri));
        rf.putFile(imageuri)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                        Toast.makeText(getApplication(),"Image Upload Success",Toast.LENGTH_LONG).show();

                        Task<Uri>uriTask=taskSnapshot.getStorage().getDownloadUrl();
                        while (!uriTask.isSuccessful());
                        Uri DownloadUrl=uriTask.getResult();

                        saveImage load=new saveImage(imagN,DownloadUrl.toString());
                        String imageId=databaseReference.push().getKey();
                        databaseReference.child(imageId).setValue(load);

                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle unsuccessful uploads
                        // ...
                        Toast.makeText(getApplication(),"Image Upload Not Success"+exception,Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void opeanfilechoore() {

        Intent intent=new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent,IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==IMAGE_REQUEST && resultCode==RESULT_OK && data!=null && data.getData()!=null){

            imageuri=data.getData();
            Picasso.with(this).load(imageuri).into(imageView);
        }
    }
}

